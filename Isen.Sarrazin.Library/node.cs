using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;


namespace Isen.Sarrazin.Library

{

    public partial class Node<T>: INode<T>

    {
        //variables
        public T Value { get; set; }

        public Guid Id{get;set;}

        public Node<T> Parent { get; set; }

        public List<Node<T>> Children { get; set; }

        public int Depth { get; }

        public Node(T value)
        {
            Value = value;
        }

        public Node(T value, Node<T> parent)
        {
            Value = value;
            Parent = parent;
        }


        public void AddChildNode(Node<T> node)
        {
            //Ajout du node � la liste d'enfants
            this.Children.Add(node);
            //on lie l'enfant au node actuel
            node.Parent = this;
        }

        public void AddNodes(IEnumerable<Node<T>> nodeList)
        {
            //Pour chaque node de la liste
            foreach (Node<T> node in nodeList)
            {
                //on ajoute l'enfant � la liste 
                this.Children.Add(node);   
                //On lie l'enfant au noeud courant en parent
                node.Parent = this;
            }
        }

        public void RemoveChildNode(Guid id)
        {
            //On parcourt la liste des enfants
            foreach (Node<T> node in this.Children)
            {
                //Si non tombe sur un node dont l'id est celui recherch�
                if (node.Id == id)
                {
                    //On le retire de la liste des enfants
                    this.Children.Remove(node);
                    //On retire le parent 
                    node.Parent = null;
                }   
            }
        }

        public void RemoveChildNode(Node<T> node)
        {
            //Pour chaque node de la liste des enfants
            foreach (Node<T> childNode in this.Children)
            {
                //Si le node est egal � celui recherch�
                if (childNode.Equals(node))
                {
                    //On le retire de la liste
                    this.Children.Remove(childNode);
                    //On passe son parent � null
                    childNode.Parent = null;
                }   
            }
        }

       public Node<T> FindTraversing(Guid id)
        {
            //Pour chaque node dans les enfants
            foreach (Node<T> node in this.Children)
            {
                //Si  on a trouv� le node recherch�
                if (node.Id == id)
                {
                    //On retourne le node en question
                    return node;
                }
                else
                {
                    //sinon on descend dans l'arbre
                    return node.FindTraversing(id);
                }
            }

            //si pas d'enfants, on est en bas de l'arbre, on retourne null
            return null;
        }

        public Node<T> FindTraversing(Node<T> node)
        {
            //M�me m�thode que la m�thode pr�c�dente avec un �galit� de node et non une �galit� d'id
            foreach (Node<T> childNode in this.Children)
            {
                if (childNode.Equals(node))
                {
                    return childNode;
                }
                else
                {
                    return childNode.FindTraversing(node);
                }
            }

            return null;
        }

        public override string ToString()
        {
            //Appelle � la fonction r�cursive � partir du node actuel consid�r� comme le niveau 0
            return this.ToString(0);
        }

        public string ToString(int niveau)
        {
            //valeur de retour
            string returnString="";

            //On ajoute autant de |- que l'on est descendu dans l'arbre
            for (int i = 0; i < niveau; i++)
            {
                 returnString += "|-";
            }

            //On ajoute la value et l'id
            returnString += this.Value + "{" + this.Id + "}\n";

            //Pour chaque enfant
            foreach (Node<T> node in this.Children)
            {
                //on descend dans l'arbre avec un niveau n+1
                returnString += node.ToString(niveau + 1);
            }

            //On retourne la valeur de retour
            return returnString;
        }

        public string JsonSerialize()
        {
            //On convertit l'objet
            string json = JsonConvert.SerializeObject(this);
            //on retourne la chaie associ�
            return json;
        }

        public void JsonDeserializeChildren(string json)
        {
            //on r�cup�re une liste de node depuis une string json
            List<Node<T>> nodes = JsonConvert.DeserializeObject<List<Node<T>>>(json);

            //pour chaque node de la liste
            foreach (Node<T> node in nodes)
            {
                //on l'ajoute � la liste d'enfants
                this.Children.Add(node);
            }
        }
    }
}