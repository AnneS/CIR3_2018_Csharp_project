﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Isen.Sarrazin.Library
{
    public partial class Node<T> : IEquatable<Node<T>>
    {
        public bool Equals(Node<T> other)
        {
            if ((object) other == null) return false;
            return Id == other.Id && Value.Equals(other.Value);

        }

        public override bool Equals(object obj)

        {

            if (obj == null) return false;

            if (!(obj is Node<T>)) return false;

            return Equals((Node<T>)obj);

        }

        public override int GetHashCode()

        {

            var hash = 17;

            hash = hash * 23 + Value.GetHashCode();

            hash = hash * 23 + Id.GetHashCode();

            return hash;

        }
    }
}
